package com.example.app.service.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class QuestionResponse {
    Long id;
    FileResponse file;
    String name;
    String answer;
    Boolean isCorrect;
    com.example.drivingexam.dto.response.SectionResponse section;
}
