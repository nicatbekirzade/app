package com.example.app.exception;

import com.crocusoft.taxes.domain.errorModel.Message;

public class CustomFeignException extends RuntimeException {

    private static final long serialVersionUID = 58432132465140L;

    public CustomFeignException(Message message) {
        super(message.toString());
    }

    public CustomFeignException(Message message, Throwable ex) {
        super(message.toString(), ex);
    }
}
