package com.example.app.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 58432132465811L;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(Object param) {
        super(String.format("Record with '%s' was not found", param));
    }

    public NotFoundException() {
        super("Record was not found");
    }

    public NotFoundException(String message, Throwable ex) {
        super(message, ex);
    }
}
